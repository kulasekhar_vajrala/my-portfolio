import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from './shared/shared.module';

const routes: Routes = [
  { path: '',   redirectTo: '/details/home', pathMatch: 'full' , runGuardsAndResolvers: 'always'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true,
    onSameUrlNavigation: 'reload',
    enableTracing: false
  }),SharedModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
