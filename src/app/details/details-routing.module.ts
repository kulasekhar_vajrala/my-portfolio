import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { HomeComponent } from './home/home.component';
import { ExperienceComponent } from './experience/experience.component';
import { ContactMeComponent } from './contact-me/contact-me.component';

import { CommonModule } from '@angular/common';
const routes: Routes = [
  {
    path: 'details',
    component: DetailsComponent,
    runGuardsAndResolvers: 'always',
    children: [
      { path: '',   redirectTo: 'home', pathMatch: 'full', runGuardsAndResolvers: 'always'},
      { path: 'home',  component: HomeComponent, runGuardsAndResolvers: 'always' },
      { path: 'experience',  component: ExperienceComponent, runGuardsAndResolvers: 'always' },
      { path: 'contact',  component: ContactMeComponent, runGuardsAndResolvers: 'always' },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'reload',
    useHash: true,
    enableTracing: false
  })],
  exports: [RouterModule]
})
export class DetailsRoutingModule { }
