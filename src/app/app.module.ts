import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './shared/shared.module';
import { ParticlesModule } from 'angular-particle';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DetailsModule } from './details/details.module';
import { AngularFireModule } from '@angular/fire';  
import { environment } from '../environments/environment';
@NgModule({
  declarations: [
    AppComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    SharedModule,
    ParticlesModule,
    BrowserAnimationsModule,
    HttpClientModule,
    DetailsModule,
    AngularFireModule.initializeApp(environment.firebase)  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
