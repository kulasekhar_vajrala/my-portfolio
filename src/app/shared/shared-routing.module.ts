import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedComponent } from './shared/shared.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { ParticlesCoverComponent } from './particles-cover/particles-cover.component'

const routes: Routes = [
  {
    path: 'shared',
    component: SharedComponent,
    runGuardsAndResolvers: 'always',
    children: [
      { path: 'navbar',  component: NavbarComponent, runGuardsAndResolvers: 'always' },
      { path: 'footer',  component: FooterComponent, runGuardsAndResolvers: 'always' },
      { path: 'cover',  component: ParticlesCoverComponent, runGuardsAndResolvers: 'always' },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SharedRoutingModule { }
