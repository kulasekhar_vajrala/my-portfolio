import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { SharedComponent } from './shared/shared.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { ParticlesCoverComponent } from './particles-cover/particles-cover.component'
import { ParticlesModule } from 'angular-particle';

@NgModule({
  declarations: [SharedComponent, NavbarComponent, FooterComponent, ParticlesCoverComponent],
  exports:[
    ParticlesCoverComponent,
    NavbarComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    ParticlesModule
  ]
})
export class SharedModule { }
