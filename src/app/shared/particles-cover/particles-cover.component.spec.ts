import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticlesCoverComponent } from './particles-cover.component';

describe('ParticlesCoverComponent', () => {
  let component: ParticlesCoverComponent;
  let fixture: ComponentFixture<ParticlesCoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticlesCoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticlesCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
