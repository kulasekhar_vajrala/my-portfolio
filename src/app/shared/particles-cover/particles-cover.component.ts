import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-particles-cover',
  templateUrl: './particles-cover.component.html',
  styleUrls: ['./particles-cover.component.css']
})
export class ParticlesCoverComponent implements OnInit {
  http;
  myStyle: object = {};
  myParams: object = {};
  width: number = 100;
  height: number = 100;
  particlesConfig: object= {};
  constructor(private _http:HttpClient) {
    this.http = _http;
   }

  ngOnInit() {
    this.myStyle = {
      'position': 'absolute',
      'width': '100%',
      'height': '100%',
      'z-index': -1,
      'top': 0,
      'left': 0,
      'right': 0,
      'bottom': 0,
      'background-color':'#161616',
      'background-size':'cover',
      'background-position':'50% 50%',
      'background-repeat':'no-repeat'
    };
    this.loadConfig();
  }
  loadConfig(){
    this.http.get('./assets/particles-config.json').subscribe(res =>{
      this.particlesConfig=res;
      console.log(res);
    });
  }
}
